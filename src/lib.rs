#![feature(extern_types)]
#![allow(non_camel_case_types)]
use std::os::raw::*;

macro_rules! impl_struct {
    ($name:ident { $($field:ident: $type:ty,)* }) => {
        #[repr(C)]
        #[derive(Clone, Copy, Debug)]
        pub struct $name { $($field: $type,)* }
        impl std::default::Default for $name {
            fn default() -> Self { unsafe { std::mem::zeroed() } }
        }
    }
}

impl_struct!(tm {
    tm_sec: c_int,
    tm_min: c_int,
    tm_hour: c_int,
    tm_mday: c_int,
    tm_mon: c_int,
    tm_year: c_int,
    tm_wday: c_int,
    tm_yday: c_int,
    tm_isdst: c_int,
    tm_gmtoff: c_long,
    tm_zone: *const c_char,
});
impl_struct!(color_struct {
    red: byte,
    green: byte,
    blue: byte,
});
impl_struct!(color_16_struct {
    index: byte,
    red: uint_16,
    green: uint_16,
    blue: uint_16,
    gray: uint_16,
});
impl_struct!(color_8_struct {
    red: byte,
    green: byte,
    blue: byte,
    gray: byte,
    alpha: byte,
});
impl_struct!(sPLT_entry_struct {
    red: uint_16,
    green: uint_16,
    blue: uint_16,
    alpha: uint_16,
    frequency: uint_16,
});
impl_struct!(sPLT_struct {
    name: charp,
    depth: byte,
    entries: sPLT_entryp,
    nentries: int_32,
});
impl_struct!(text_struct {
    compression: c_int,
    key: charp,
    text: charp,
    text_length: usize,
    itxt_length: usize,
    lang: charp,
    lang_key: charp,
});
impl_struct!(time_struct {
    year: uint_16,
    month: byte,
    day: byte,
    hour: byte,
    minute: byte,
    second: byte,
});
impl_struct!(unknown_chunk_t {
    name: [byte; 5usize],
    data: *mut byte,
    size: usize,
    location: byte,
});
impl_struct!(row_info_struct {
    width: uint_32,
    rowbytes: usize,
    color_type: byte,
    bit_depth: byte,
    channels: byte,
    pixel_depth: byte,
});

#[repr(C)]
#[derive(Clone, Copy)]
pub struct image {
    pub opaque: controlp,
    pub version: uint_32,
    pub width: uint_32,
    pub height: uint_32,
    pub format: uint_32,
    pub flags: uint_32,
    pub colormap_entries: uint_32,
    pub warning_or_error: uint_32,
    pub message: [c_char; 64usize],
}

impl std::fmt::Debug for image {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.debug_struct("image")
            .field("opaque", &self.opaque)
            .field("version", &self.version)
            .field("width", &self.width)
            .field("height", &self.height)
            .field("format", &self.format)
            .field("flags", &self.flags)
            .field("colormap_entries", &self.colormap_entries)
            .field("warning_or_error", &self.warning_or_error)
            .field("message", &"[...]")
            .finish()
    }
}

impl std::default::Default for image {
    fn default() -> Self { unsafe { std::mem::zeroed() } }
}

extern {
    pub type FILE;
    pub type jmp_buf;
    pub type struct_def;
    pub type info_def;
    pub type control;
}

pub type time_t = c_long;
pub type byte = c_uchar;
pub type int_16 = c_short;
pub type uint_16 = c_ushort;
pub type int_32 = c_int;
pub type uint_32 = c_uint;
pub type size_t = usize;
pub type ptrdiff_t = isize;
pub type alloc_size_t = usize;
pub type fixed_point = int_32;
pub type voidp = *mut c_void;
pub type const_voidp = *const c_void;
pub type bytep = *mut byte;
pub type const_bytep = *const byte;
pub type uint_32p = *mut uint_32;
pub type const_uint_32p = *const uint_32;
pub type int_32p = *mut int_32;
pub type const_int_32p = *const int_32;
pub type uint_16p = *mut uint_16;
pub type const_uint_16p = *const uint_16;
pub type int_16p = *mut int_16;
pub type const_int_16p = *const int_16;
pub type charp = *mut c_char;
pub type const_charp = *const c_char;
pub type fixed_point_p = *mut fixed_point;
pub type const_fixed_point_p = *const fixed_point;
pub type size_tp = *mut usize;
pub type const_size_tp = *const usize;
pub type FILE_p = *mut FILE;
pub type doublep = *mut f64;
pub type const_doublep = *const f64;
pub type bytepp = *mut *mut byte;
pub type uint_32pp = *mut *mut uint_32;
pub type int_32pp = *mut *mut int_32;
pub type uint_16pp = *mut *mut uint_16;
pub type int_16pp = *mut *mut int_16;
pub type const_charpp = *mut *const c_char;
pub type charpp = *mut *mut c_char;
pub type fixed_point_pp = *mut *mut fixed_point;
pub type doublepp = *mut *mut f64;
pub type charppp = *mut *mut *mut c_char;
pub type libpng_version_1_6_36 = *mut c_char;
pub type const_structp = *const struct_def;
pub type structp = *mut struct_def;
pub type structpp = *mut *mut struct_def;
pub type info = info_def;
pub type infop = *mut info;
pub type const_infop = *const info;
pub type infopp = *mut *mut info;
pub type structrp = *mut struct_def;
pub type const_structrp = *const struct_def;
pub type inforp = *mut info;
pub type const_inforp = *const info;
pub type color = color_struct;
pub type colorp = *mut color;
pub type const_colorp = *const color;
pub type colorpp = *mut *mut color;
pub type color_16 = color_16_struct;
pub type color_16p = *mut color_16;
pub type const_color_16p = *const color_16;
pub type color_16pp = *mut *mut color_16;
pub type color_8 = color_8_struct;
pub type color_8p = *mut color_8;
pub type const_color_8p = *const color_8;
pub type color_8pp = *mut *mut color_8;
pub type sPLT_entry = sPLT_entry_struct;
pub type sPLT_entryp = *mut sPLT_entry;
pub type const_sPLT_entryp = *const sPLT_entry;
pub type sPLT_entrypp = *mut *mut sPLT_entry;
pub type sPLT_t = sPLT_struct;
pub type sPLT_tp = *mut sPLT_t;
pub type const_sPLT_tp = *const sPLT_t;
pub type sPLT_tpp = *mut *mut sPLT_t;
pub type text = text_struct;
pub type textp = *mut text;
pub type const_textp = *const text;
pub type textpp = *mut *mut text;
pub type time = time_struct;
pub type timep = *mut time;
pub type const_timep = *const time;
pub type timepp = *mut *mut time;
pub type unknown_chunk = unknown_chunk_t;
pub type unknown_chunkp = *mut unknown_chunk;
pub type const_unknown_chunkp = *const unknown_chunk;
pub type unknown_chunkpp = *mut *mut unknown_chunk;
pub type row_info = row_info_struct;
pub type row_infop = *mut row_info;
pub type row_infopp = *mut *mut row_info;
pub type controlp = *mut control;
pub type imagep = *mut image;

macro_rules! define {
    ($name:ident, $val:expr) => {
        #[macro_export]
        macro_rules! $name { () => { $val } }
    }
}

define!(DISPOSE_OP_NONE, 0x00);
define!(DISPOSE_OP_BACKGROUND, 0x01);
define!(DISPOSE_OP_PREVIOUS, 0x02);
define!(BLEND_OP_SOURCE, 0x00);
define!(BLEND_OP_OVER, 0x01);
define!(TEXT_COMPRESSION_NONE_WR, -3);
define!(TEXT_COMPRESSION_zTXt_WR, -2);
define!(TEXT_COMPRESSION_NONE, -1);
define!(TEXT_COMPRESSION_zTXt, 0);
define!(ITXT_COMPRESSION_NONE, 1);
define!(ITXT_COMPRESSION_zTXt, 2);
define!(TEXT_COMPRESSION_LAST, 3);
define!(HAVE_IHDR, 0x01);
define!(HAVE_PLTE, 0x02);
define!(AFTER_IDAT, 0x08);
define!(FP_1, 100000);
define!(FP_HALF, 50000);
define!(FP_MAX, 0x7fffffff);
define!(FP_MIN, -FP_MAX!());
define!(COLOR_MASK_PALETTE, 1);
define!(COLOR_MASK_COLOR, 2);
define!(COLOR_MASK_ALPHA, 4);
define!(COLOR_TYPE_GRAY, 0);
define!(COLOR_TYPE_PALETTE, COLOR_MASK_COLOR!() | COLOR_MASK_PALETTE!());
define!(COLOR_TYPE_RGB, COLOR_MASK_COLOR!());
define!(COLOR_TYPE_RGB_ALPHA, COLOR_MASK_COLOR!() | COLOR_MASK_ALPHA!());
define!(COLOR_TYPE_GRAY_ALPHA, COLOR_MASK_ALPHA!());
define!(COLOR_TYPE_RGBA, COLOR_TYPE_RGB_ALPHA!());
define!(COLOR_TYPE_GA, COLOR_TYPE_GRAY_ALPHA!());
define!(COMPRESSION_TYPE_BASE, 0);
define!(COMPRESSION_TYPE_DEFAULT, COMPRESSION_TYPE_BASE!());
define!(FILTER_TYPE_BASE, 0);
define!(INTRAPIXEL_DIFFERENCING, 64);
define!(FILTER_TYPE_DEFAULT, FILTER_TYPE_BASE!());
define!(INTERLACE_NONE, 0);
define!(INTERLACE_ADAM7, 1);
define!(INTERLACE_LAST, 2);
define!(OFFSET_PIXEL, 0);
define!(OFFSET_MICROMETER, 1);
define!(OFFSET_LAST, 2);
define!(EQUATION_LINEAR, 0);
define!(EQUATION_BASE_E, 1);
define!(EQUATION_ARBITRARY, 2);
define!(EQUATION_HYPERBOLIC, 3);
define!(EQUATION_LAST, 4);
define!(SCALE_UNKNOWN, 0);
define!(SCALE_METER, 1);
define!(SCALE_RADIAN, 2);
define!(SCALE_LAST, 3);
define!(RESOLUTION_UNKNOWN, 0);
define!(RESOLUTION_METER, 1);
define!(RESOLUTION_LAST, 2);
define!(sRGB_INTENT_PERCEPTUAL, 0);
define!(sRGB_INTENT_RELATIVE, 1);
define!(sRGB_INTENT_SATURATION, 2);
define!(sRGB_INTENT_ABSOLUTE, 3);
define!(sRGB_INTENT_LAST, 4);
define!(KEYWORD_MAX_LENGTH, 79);
define!(MAX_PALETTE_LENGTH, 256);
define!(INFO_gAMA, 0x0001);
define!(INFO_sBIT, 0x0002);
define!(INFO_cHRM, 0x0004);
define!(INFO_PLTE, 0x0008);
define!(INFO_tRNS, 0x0010);
define!(INFO_bKGD, 0x0020);
define!(INFO_hIST, 0x0040);
define!(INFO_pHYs, 0x0080);
define!(INFO_oFFs, 0x0100);
define!(INFO_tIME, 0x0200);
define!(INFO_pCAL, 0x0400);
define!(INFO_sRGB, 0x0800);
define!(INFO_iCCP, 0x1000);
define!(INFO_sPLT, 0x2000);
define!(INFO_sCAL, 0x4000);
define!(INFO_IDAT, 0x8000);
define!(INFO_eXIf, 0x10000);
define!(INFO_acTL, 0x20000);
define!(INFO_fcTL, 0x40000);
define!(TRANSFORM_IDENTITY, 0x0000);
define!(TRANSFORM_STRIP_16, 0x0001);
define!(TRANSFORM_STRIP_ALPHA, 0x0002);
define!(TRANSFORM_PACKING, 0x0004);
define!(TRANSFORM_PACKSWAP, 0x0008);
define!(TRANSFORM_EXPAND, 0x0010);
define!(TRANSFORM_INVERT_MONO, 0x0020);
define!(TRANSFORM_SHIFT, 0x0040);
define!(TRANSFORM_BGR, 0x0080);
define!(TRANSFORM_SWAP_ALPHA, 0x0100);
define!(TRANSFORM_SWAP_ENDIAN, 0x0200);
define!(TRANSFORM_INVERT_ALPHA, 0x0400);
define!(TRANSFORM_STRIP_FILLER, 0x0800);
define!(TRANSFORM_STRIP_FILLER_BEFORE, TRANSFORM_STRIP_FILLER!());
define!(TRANSFORM_STRIP_FILLER_AFTER, 0x1000);
define!(TRANSFORM_GRAY_TO_RGB, 0x2000);
define!(TRANSFORM_EXPAND_16, 0x4000);
define!(TRANSFORM_SCALE_16, 0x8000);
define!(FLAG_MNG_EMPTY_PLTE, 0x01);
define!(FLAG_MNG_FILTER_64, 0x04);
define!(ALL_MNG_FEATURES, 0x05);
define!(ERROR_ACTION_NONE, 1);
define!(ERROR_ACTION_WARN, 2);
define!(ERROR_ACTION_ERROR, 3);
define!(RGB_TO_GRAY_DEFAULT, -1);
define!(ALPHA_PNG, 0);
define!(ALPHA_STANDARD, 1);
define!(ALPHA_ASSOCIATED, 1);
define!(ALPHA_PREMULTIPLIED, 1);
define!(ALPHA_OPTIMIZED, 2);
define!(ALPHA_BROKEN, 3);
define!(DEFAULT_sRGB, -1);
define!(GAMMA_MAC_18, -2);
define!(GAMMA_sRGB, 220000);
define!(GAMMA_LINEAR, FP_1);
define!(GAMMA_THRESHOLD, GAMMA_THRESHOLD_FIXED!() as _ * 0.00001);
define!(CRC_DEFAULT, 0);
define!(CRC_ERROR_QUIT, 1);
define!(CRC_WARN_DISCARD, 2);
define!(CRC_WARN_USE, 3);
define!(CRC_QUIET_USE, 4);
define!(CRC_NO_CHANGE, 5);
define!(NO_FILTERS, 0x00);
define!(FILTER_NONE, 0x08);
define!(FILTER_SUB, 0x10);
define!(FILTER_UP, 0x20);
define!(FILTER_AVG, 0x40);
define!(FILTER_PAETH, 0x80);
define!(FAST_FILTERS, FILTER_NONE!() | FILTER_SUB!() | FILTER_UP!());
define!(ALL_FILTERS, FAST_FILTERS!() | FILTER_AVG!() | FILTER_PAETH!());
define!(FILTER_VALUE_NONE, 0);
define!(FILTER_VALUE_SUB, 1);
define!(FILTER_VALUE_UP, 2);
define!(FILTER_VALUE_AVG, 3);
define!(FILTER_VALUE_PAETH, 4);
define!(FILTER_VALUE_LAST, 5);
define!(FILTER_HEURISTIC_DEFAULT, 0);
define!(FILTER_HEURISTIC_UNWEIGHTED, 1);
define!(FILTER_HEURISTIC_WEIGHTED, 2);
define!(FILTER_HEURISTIC_LAST, 3);
define!(DESTROY_WILL_FREE_DATA, 1);
define!(SET_WILL_FREE_DATA, 1);
define!(USER_WILL_FREE_DATA, 2);
define!(FREE_HIST, 0x0008);
define!(FREE_ICCP, 0x0010);
define!(FREE_SPLT, 0x0020);
define!(FREE_ROWS, 0x0040);
define!(FREE_PCAL, 0x0080);
define!(FREE_SCAL, 0x0100);
define!(FREE_PLTE, 0x1000);
define!(FREE_TRNS, 0x2000);
define!(FREE_TEXT, 0x4000);
define!(FREE_EXIF, 0x8000);
define!(FREE_ALL, 0xffff);
define!(FREE_MUL, 0x4220);
define!(HANDLE_CHUNK_AS_DEFAULT, 0);
define!(HANDLE_CHUNK_NEVER, 1);
define!(HANDLE_CHUNK_IF_SAFE, 2);
define!(HANDLE_CHUNK_ALWAYS, 3);
define!(HANDLE_CHUNK_LAST, 4);
define!(INTERLACE_ADAM7_PASSES, 7);
define!(IMAGE_VERSION, 1);
define!(FORMAT_FLAG_ALPHA, 0x01);
define!(FORMAT_FLAG_COLOR, 0x02);
define!(FORMAT_FLAG_LINEAR, 0x04);
define!(FORMAT_FLAG_COLORMAP, 0x08);
define!(FORMAT_FLAG_ASSOCIATED_ALPHA, 0x40);
define!(FORMAT_GRAY, 0);
define!(FORMAT_GA, FORMAT_FLAG_ALPHA!());
define!(FORMAT_AG, FORMAT_GA!() | FORMAT_FLAG_AFIRST!());
define!(FORMAT_RGB, FORMAT_FLAG_COLOR!());
define!(FORMAT_BGR, FORMAT_FLAG_COLOR!() | FORMAT_FLAG_BGR!());
define!(FORMAT_RGBA, FORMAT_RGB!() | FORMAT_FLAG_ALPHA!());
define!(FORMAT_ARGB, FORMAT_RGBA!() | FORMAT_FLAG_AFIRST!());
define!(FORMAT_BGRA, FORMAT_BGR!() | FORMAT_FLAG_ALPHA!());
define!(FORMAT_ABGR, FORMAT_BGRA!() | FORMAT_FLAG_AFIRST!());
define!(FORMAT_LINEAR_Y, FORMAT_FLAG_LINEAR!());
define!(FORMAT_LINEAR_Y_ALPHA, FORMAT_FLAG_LINEAR!() | FORMAT_FLAG_ALPHA!());
define!(FORMAT_LINEAR_RGB, FORMAT_FLAG_LINEAR!() | FORMAT_FLAG_COLOR!());
define!(FORMAT_LINEAR_RGB_ALPHA, PNG_FORMAT_FLAG_LINEAR!() | PNG_FORMAT_FLAG_COLOR!() | PNG_FORMAT_FLAG_ALPHA!());
define!(FORMAT_RGB_COLORMAP, FORMAT_RGB!() | FORMAT_FLAG_COLORMAP!());
define!(FORMAT_BGR_COLORMAP, FORMAT_BGR!() | FORMAT_FLAG_COLORMAP!());
define!(FORMAT_RGBA_COLORMAP, FORMAT_RGBA!() | FORMAT_FLAG_COLORMAP!());
define!(FORMAT_ARGB_COLORMAP, FORMAT_ARGB!() | FORMAT_FLAG_COLORMAP!());
define!(FORMAT_BGRA_COLORMAP, FORMAT_BGRA!() | FORMAT_FLAG_COLORMAP!());
define!(FORMAT_ABGR_COLORMAP, FORMAT_ABGR!() | FORMAT_FLAG_COLORMAP!());
define!(IMAGE_FLAG_COLORSPACE_NOT_sRGB, 0x01);
define!(IMAGE_FLAG_FAST, 0x02);
define!(IMAGE_FLAG_16BIT_sRGB, 0x04);
define!(MAXIMUM_INFLATE_WINDOW, 2);
define!(SKIP_sRGB_CHECK_PROFILE, 4);
define!(IGNORE_ADLER32, 8);
define!(OPTION_NEXT, 12);
define!(OPTION_UNSET, 0);
define!(OPTION_INVALID, 1);
define!(OPTION_OFF, 2);
define!(OPTION_ON, 3);
define!(FILLER_BEFORE, 0);
define!(FILLER_AFTER, 1);
define!(BACKGROUND_GAMMA_UNKNOWN, 0);
define!(BACKGROUND_GAMMA_SCREEN, 1);
define!(BACKGROUND_GAMMA_FILE, 2);
define!(BACKGROUND_GAMMA_UNIQUE, 3);
define!(FREE_UNKN, 0x0200);
define!(IO_NONE, 0x0000);
define!(IO_READING, 0x0001);
define!(IO_WRITING, 0x0002);
define!(IO_SIGNATURE, 0x0010);
define!(IO_CHUNK_HDR, 0x0020);
define!(IO_CHUNK_DATA, 0x0040);
define!(IO_CHUNK_CRC, 0x0080);
define!(IO_MASK_OP, 0x000f);
define!(IO_MASK_LOC, 0x00f0);
define!(IMAGE_WARNING, 1);
define!(IMAGE_ERROR, 2);
define!(FORMAT_FLAG_BGR, 0x10);
define!(FORMAT_FLAG_AFIRST, 0x20);
define!(ARM_NEON, 0);
define!(MIPS_MSA, 6);
define!(POWERPC_VSX, 10);

macro_rules! impl_fn_ptr {
    ($name:ident($($arg:ident: $type:ty,)*) $(-> $ret:ty)*) => {
        pub type $name = std::option::Option<
            unsafe extern "C" fn($($arg: $type,)*) $(-> $ret)*
        >;
    }
}
impl_fn_ptr!(error_ptr(
    arg1: structp,
    arg2: const_charp,
));
impl_fn_ptr!(rw_ptr(
    arg1: structp,
    arg2: bytep,
    arg3: usize,
));
impl_fn_ptr!(flush_ptr(
    arg1: structp,
));
impl_fn_ptr!(read_status_ptr(
    arg1: structp,
    arg2: uint_32,
    arg3: c_int,
));
impl_fn_ptr!(write_status_ptr(
    arg1: structp,
    arg2: uint_32,
    arg3: c_int,
));
impl_fn_ptr!(progressive_info_ptr(
    arg1: structp,
    arg2: infop,
));
impl_fn_ptr!(progressive_end_ptr(
    arg1: structp,
    arg2: infop,
));
impl_fn_ptr!(progressive_frame_ptr(
    arg1: structp,
    arg2: uint_32,
));
impl_fn_ptr!(progressive_row_ptr(
    arg1: structp,
    arg2: bytep,
    arg3: uint_32,
    arg4: c_int,
));
impl_fn_ptr!(user_transform_ptr(
    arg1: structp,
    arg2: row_infop,
    arg3: bytep,
));
impl_fn_ptr!(user_chunk_ptr(
    arg1: structp,
    arg2: unknown_chunkp,
) -> c_int);
impl_fn_ptr!(longjmp_ptr(
    arg1: *mut jmp_buf,
    arg2: c_int,
));
impl_fn_ptr!(malloc_ptr(
    arg1: structp,
    arg2: alloc_size_t,
) -> voidp);
impl_fn_ptr!(free_ptr(
    arg1: structp,
    arg2: voidp,
));

#[link(name = "png")]
extern "C" {
    #[link_name = "png_access_version_number"]
    pub fn access_version_number(
    ) -> uint_32;
    #[link_name = "png_set_sig_bytes"]
    pub fn set_sig_bytes(
        ptr: structrp,
        num_bytes: c_int,
    );
    #[link_name = "png_sig_cmp"]
    pub fn sig_cmp(
        sig: const_bytep,
        start: usize,
        num_to_check: usize,
    ) -> c_int;
    #[link_name = "png_create_read_struct"]
    pub fn create_read_struct(
        user_png_ver: const_charp,
        error_ptr: voidp,
        error_fn: error_ptr,
        warn_fn: error_ptr,
    ) -> structp;
    #[link_name = "png_create_write_struct"]
    pub fn create_write_struct(
        user_png_ver: const_charp,
        error_ptr: voidp,
        error_fn: error_ptr,
        warn_fn: error_ptr,
    ) -> structp;
    #[link_name = "png_get_compression_buffer_size"]
    pub fn get_compression_buffer_size(
        ptr: const_structrp,
    ) -> usize;
    #[link_name = "png_set_compression_buffer_size"]
    pub fn set_compression_buffer_size(
        ptr: structrp,
        size: usize,
    );
    #[link_name = "png_set_longjmp_fn"]
    pub fn set_longjmp_fn(
        ptr: structrp,
        longjmp_fn: longjmp_ptr,
        jmp_buf_size: usize,
    ) -> *mut jmp_buf;
    #[link_name = "png_longjmp"]
    pub fn longjmp(
        ptr: const_structrp,
        val: c_int,
    );
    #[link_name = "png_reset_zstream"]
    pub fn reset_zstream(
        ptr: structrp,
    ) -> c_int;
    #[link_name = "png_create_read_struct_2"]
    pub fn create_read_struct_2(
        user_png_ver: const_charp,
        error_ptr: voidp,
        error_fn: error_ptr,
        warn_fn: error_ptr,
        mem_ptr: voidp,
        malloc_fn: malloc_ptr,
        free_fn: free_ptr,
    ) -> structp;
    #[link_name = "png_create_write_struct_2"]
    pub fn create_write_struct_2(
        user_png_ver: const_charp,
        error_ptr: voidp,
        error_fn: error_ptr,
        warn_fn: error_ptr,
        mem_ptr: voidp,
        malloc_fn: malloc_ptr,
        free_fn: free_ptr,
    ) -> structp;
    #[link_name = "png_write_sig"]
    pub fn write_sig(
        ptr: structrp,
    );
    #[link_name = "png_write_chunk"]
    pub fn write_chunk(
        ptr: structrp,
        chunk_name: const_bytep,
        data: const_bytep,
        length: usize,
    );
    #[link_name = "png_write_chunk_start"]
    pub fn write_chunk_start(
        ptr: structrp,
        chunk_name: const_bytep,
        length: uint_32,
    );
    #[link_name = "png_write_chunk_data"]
    pub fn write_chunk_data(
        ptr: structrp,
        data: const_bytep,
        length: usize,
    );
    #[link_name = "png_write_chunk_end"]
    pub fn write_chunk_end(
        ptr: structrp,
    );
    #[link_name = "png_create_info_struct"]
    pub fn create_info_struct(
        ptr: const_structrp,
    ) -> infop;
    #[link_name = "png_info_init_3"]
    pub fn info_init_3(
        info_ptr: infopp,
        info_struct_size: usize,
    );
    #[link_name = "png_write_info_before_PLTE"]
    pub fn write_info_before_PLTE(
        ptr: structrp,
        info_ptr: const_inforp,
    );
    #[link_name = "png_write_info"]
    pub fn write_info(
        ptr: structrp,
        info_ptr: const_inforp,
    );
    #[link_name = "png_read_info"]
    pub fn read_info(
        ptr: structrp,
        info_ptr: inforp,
    );
    #[link_name = "png_convert_to_rfc1123"]
    pub fn convert_to_rfc1123(
        ptr: structrp,
        ptime: const_timep,
    )
        -> const_charp;
        #[link_name = "png_convert_to_rfc1123_buffer"]
    pub fn convert_to_rfc1123_buffer(
        out: *mut c_char,
        ptime: const_timep,
    ) -> c_int;
    #[link_name = "png_convert_from_struct_tm"]
    pub fn convert_from_struct_tm(
        ptime: timep,
        ttime: *const tm,
    );
    #[link_name = "png_convert_from_time_t"]
    pub fn convert_from_time_t(
        ptime: timep,
        ttime: time_t,
    );
    #[link_name = "png_set_expand"]
    pub fn set_expand(
        ptr: structrp,
    );
    #[link_name = "png_set_expand_gray_1_2_4_to_8"]
    pub fn set_expand_gray_1_2_4_to_8(
        ptr: structrp,
    );
    #[link_name = "png_set_palette_to_rgb"]
    pub fn set_palette_to_rgb(
        ptr: structrp,
    );
    #[link_name = "png_set_tRNS_to_alpha"]
    pub fn set_tRNS_to_alpha(
        ptr: structrp,
    );
    #[link_name = "png_set_expand_16"]
    pub fn set_expand_16(
        ptr: structrp,
    );
    #[link_name = "png_set_bgr"]
    pub fn set_bgr(
        ptr: structrp,
    );
    #[link_name = "png_set_gray_to_rgb"]
    pub fn set_gray_to_rgb(
        ptr: structrp,
    );
    #[link_name = "png_set_rgb_to_gray"]
    pub fn set_rgb_to_gray(
        ptr: structrp,
        error_action: c_int,
        red: f64,
        green: f64,
    );
    #[link_name = "png_set_rgb_to_gray_fixed"]
    pub fn set_rgb_to_gray_fixed(
        ptr: structrp,
        error_action: c_int,
        red: fixed_point,
        green: fixed_point,
    );
    #[link_name = "png_get_rgb_to_gray_status"]
    pub fn get_rgb_to_gray_status(
        ptr: const_structrp,
    ) -> byte;
    #[link_name = "png_build_grayscale_palette"]
    pub fn build_grayscale_palette(
        bit_depth: c_int,
        palette: colorp,
    );
    #[link_name = "png_set_alpha_mode"]
    pub fn set_alpha_mode(
        ptr: structrp,
        mode: c_int,
        output_gamma: f64,
    );
    #[link_name = "png_set_alpha_mode_fixed"]
    pub fn set_alpha_mode_fixed(
        ptr: structrp,
        mode: c_int,
        output_gamma: fixed_point,
    );
    #[link_name = "png_set_strip_alpha"]
    pub fn set_strip_alpha(
        ptr: structrp,
    );
    #[link_name = "png_set_swap_alpha"]
    pub fn set_swap_alpha(
        ptr: structrp,
    );
    #[link_name = "png_set_invert_alpha"]
    pub fn set_invert_alpha(
        ptr: structrp,
    );
    #[link_name = "png_set_filler"]
    pub fn set_filler(
        ptr: structrp,
        filler: uint_32,
        flags: c_int,
    );
    #[link_name = "png_set_add_alpha"]
    pub fn set_add_alpha(
        ptr: structrp,
        filler: uint_32,
        flags: c_int,
    );
    #[link_name = "png_set_swap"]
    pub fn set_swap(
        ptr: structrp,
    );
    #[link_name = "png_set_packing"]
    pub fn set_packing(
        ptr: structrp,
    );
    #[link_name = "png_set_packswap"]
    pub fn set_packswap(
        ptr: structrp,
    );
    #[link_name = "png_set_shift"]
    pub fn set_shift(
        ptr: structrp,
        true_bits: const_color_8p,
    );
    #[link_name = "png_set_interlace_handling"]
    pub fn set_interlace_handling(
        ptr: structrp,
    ) -> c_int;
    #[link_name = "png_set_invert_mono"]
    pub fn set_invert_mono(
        ptr: structrp,
    );
    #[link_name = "png_set_background"]
    pub fn set_background(
        ptr: structrp,
        background_color: const_color_16p,
        background_gamma_code: c_int,
        need_expand: c_int,
        background_gamma: f64,
    );
    #[link_name = "png_set_background_fixed"]
    pub fn set_background_fixed(
        ptr: structrp,
        background_color: const_color_16p,
        background_gamma_code: c_int,
        need_expand: c_int,
        background_gamma: fixed_point,
    );
    #[link_name = "png_set_scale_16"]
    pub fn set_scale_16(
        ptr: structrp,
    );
    #[link_name = "png_set_strip_16"]
    pub fn set_strip_16(
        ptr: structrp,
    );
    #[link_name = "png_set_quantize"]
    pub fn set_quantize(
        ptr: structrp,
        palette: colorp,
        num_palette: c_int,
        maximum_colors: c_int,
        histogram: const_uint_16p,
        full_quantize: c_int,
    );
    #[link_name = "png_set_gamma"]
    pub fn set_gamma(
        ptr: structrp,
        screen_gamma: f64,
        override_file_gamma: f64,
    );
    #[link_name = "png_set_gamma_fixed"]
    pub fn set_gamma_fixed(
        ptr: structrp,
        screen_gamma: fixed_point,
        override_file_gamma: fixed_point,
    );
    #[link_name = "png_set_flush"]
    pub fn set_flush(
        ptr: structrp,
        nrows: c_int,
    );
    #[link_name = "png_write_flush"]
    pub fn write_flush(
        ptr: structrp,
    );
    #[link_name = "png_start_read_image"]
    pub fn start_read_image(
        ptr: structrp,
    );
    #[link_name = "png_read_update_info"]
    pub fn read_update_info(
        ptr: structrp,
        info_ptr: inforp,
    );
    #[link_name = "png_read_rows"]
    pub fn read_rows(
        ptr: structrp,
        row: bytepp,
        display_row: bytepp,
        num_rows: uint_32,
    );
    #[link_name = "png_read_row"]
    pub fn read_row(
        ptr: structrp,
        row: bytep,
        display_row: bytep,
    );
    #[link_name = "png_read_image"]
    pub fn read_image(
        ptr: structrp,
        image: bytepp,
    );
    #[link_name = "png_write_row"]
    pub fn write_row(
        ptr: structrp,
        row: const_bytep,
    );
    #[link_name = "png_write_rows"]
    pub fn write_rows(
        ptr: structrp,
        row: bytepp,
        num_rows: uint_32,
    );
    #[link_name = "png_write_image"]
    pub fn write_image(
        ptr: structrp,
        image: bytepp,
    );
    #[link_name = "png_write_end"]
    pub fn write_end(
        ptr: structrp,
        info_ptr: inforp,
    );
    #[link_name = "png_read_end"]
    pub fn read_end(
        ptr: structrp,
        info_ptr: inforp,
    );
    #[link_name = "png_destroy_info_struct"]
    pub fn destroy_info_struct(
        ptr: const_structrp,
        info_ptr_ptr: infopp,
    );
    #[link_name = "png_destroy_read_struct"]
    pub fn destroy_read_struct(
        ptr_ptr: structpp,
        info_ptr_ptr: infopp,
        end_info_ptr_ptr: infopp,
    );
    #[link_name = "png_destroy_write_struct"]
    pub fn destroy_write_struct(
        ptr_ptr: structpp,
        info_ptr_ptr: infopp,
    );
    #[link_name = "png_set_crc_action"]
    pub fn set_crc_action(
        ptr: structrp,
        crit_action: c_int,
        ancil_action: c_int,
    );
    #[link_name = "png_set_filter"]
    pub fn set_filter(
        ptr: structrp,
        method: c_int,
        filters: c_int,
    );
    #[link_name = "png_set_filter_heuristics"]
    pub fn set_filter_heuristics(
        ptr: structrp,
        heuristic_method: c_int,
        num_weights: c_int,
        filter_weights: const_doublep,
        filter_costs: const_doublep,
    );
    #[link_name = "png_set_filter_heuristics_fixed"]
    pub fn set_filter_heuristics_fixed(
        ptr: structrp,
        heuristic_method: c_int,
        num_weights: c_int,
        filter_weights: const_fixed_point_p,
        filter_costs: const_fixed_point_p,
    );
    #[link_name = "png_set_compression_level"]
    pub fn set_compression_level(
        ptr: structrp,
        level: c_int,
    );
    #[link_name = "png_set_compression_mem_level"]
    pub fn set_compression_mem_level(
        ptr: structrp,
        mem_level: c_int,
    );
    #[link_name = "png_set_compression_strategy"]
    pub fn set_compression_strategy(
        ptr: structrp,
        strategy: c_int,
    );
    #[link_name = "png_set_compression_window_bits"]
    pub fn set_compression_window_bits(
        ptr: structrp,
        window_bits: c_int,
    );
    #[link_name = "png_set_compression_method"]
    pub fn set_compression_method(
        ptr: structrp,
        method: c_int,
    );
    #[link_name = "png_set_text_compression_level"]
    pub fn set_text_compression_level(
        ptr: structrp,
        level: c_int,
    );
    #[link_name = "png_set_text_compression_mem_level"]
    pub fn set_text_compression_mem_level(
        ptr: structrp,
        mem_level: c_int,
    );
    #[link_name = "png_set_text_compression_strategy"]
    pub fn set_text_compression_strategy(
        ptr: structrp,
        strategy: c_int,
    );
    #[link_name = "png_set_text_compression_window_bits"]
    pub fn set_text_compression_window_bits(
        ptr: structrp,
        window_bits: c_int,
    );
    #[link_name = "png_set_text_compression_method"]
    pub fn set_text_compression_method(
        ptr: structrp,
        method: c_int,
    );
    #[link_name = "png_init_io"]
    pub fn init_io(
        ptr: structrp,
        fp: FILE_p,
    );
    #[link_name = "png_set_error_fn"]
    pub fn set_error_fn(
        ptr: structrp,
        error_ptr: voidp,
        error_fn: error_ptr,
        warning_fn: error_ptr,
    );
    #[link_name = "png_get_error_ptr"]
    pub fn get_error_ptr(
        ptr: const_structrp,
    ) -> voidp;
    #[link_name = "png_set_write_fn"]
    pub fn set_write_fn(
        ptr: structrp,
        io_ptr: voidp,
        write_data_fn: rw_ptr,
        output_flush_fn: flush_ptr,
    );
    #[link_name = "png_set_read_fn"]
    pub fn set_read_fn(
        ptr: structrp,
        io_ptr: voidp,
        read_data_fn: rw_ptr,
    );
    #[link_name = "png_get_io_ptr"]
    pub fn get_io_ptr(
        ptr: const_structrp,
    ) -> voidp;
    #[link_name = "png_set_read_status_fn"]
    pub fn set_read_status_fn(
        ptr: structrp,
        read_row_fn: read_status_ptr,
    );
    #[link_name = "png_set_write_status_fn"]
    pub fn set_write_status_fn(
        ptr: structrp,
        write_row_fn: write_status_ptr,
    );
    #[link_name = "png_set_mem_fn"]
    pub fn set_mem_fn(
        ptr: structrp,
        mem_ptr: voidp,
        malloc_fn: malloc_ptr,
        free_fn: free_ptr,
    );
    #[link_name = "png_get_mem_ptr"]
    pub fn get_mem_ptr(
        ptr: const_structrp,
    ) -> voidp;
    #[link_name = "png_set_read_user_transform_fn"]
    pub fn set_read_user_transform_fn(
        ptr: structrp,
        read_user_transform_fn: user_transform_ptr,
    );
    #[link_name = "png_set_write_user_transform_fn"]
    pub fn set_write_user_transform_fn(
        ptr: structrp,
        write_user_transform_fn: user_transform_ptr,
    );
    #[link_name = "png_set_user_transform_info"]
    pub fn set_user_transform_info(
        ptr: structrp,
        user_transform_ptr: voidp,
        user_transform_depth: c_int,
        user_transform_channels: c_int,
    );
    #[link_name = "png_get_user_transform_ptr"]
    pub fn get_user_transform_ptr(
        ptr: const_structrp,
    ) -> voidp;
    #[link_name = "png_get_current_row_number"]
    pub fn get_current_row_number(
        arg1: const_structrp,
    ) -> uint_32;
    #[link_name = "png_get_current_pass_number"]
    pub fn get_current_pass_number(
        arg1: const_structrp,
    ) -> byte;
    #[link_name = "png_set_read_user_chunk_fn"]
    pub fn set_read_user_chunk_fn(
        ptr: structrp,
        user_chunk_ptr: voidp,
        read_user_chunk_fn: user_chunk_ptr,
    );
    #[link_name = "png_get_user_chunk_ptr"]
    pub fn get_user_chunk_ptr(
        ptr: const_structrp,
    ) -> voidp;
    #[link_name = "png_set_progressive_read_fn"]
    pub fn set_progressive_read_fn(
        ptr: structrp,
        progressive_ptr: voidp,
        info_fn: progressive_info_ptr,
        row_fn: progressive_row_ptr,
        end_fn: progressive_end_ptr,
    );
    #[link_name = "png_get_progressive_ptr"]
    pub fn get_progressive_ptr(
        ptr: const_structrp,
    ) -> voidp;
    #[link_name = "png_process_data"]
    pub fn process_data(
        ptr: structrp,
        info_ptr: inforp,
        buffer: bytep,
        buffer_size: usize,
    );
    #[link_name = "png_process_data_pause"]
    pub fn process_data_pause(
        arg1: structrp,
        save: c_int,
    ) -> usize;
    #[link_name = "png_process_data_skip"]
    pub fn process_data_skip(
        arg1: structrp,
    ) -> uint_32;
    #[link_name = "png_progressive_combine_row"]
    pub fn progressive_combine_row(
        ptr: const_structrp,
        old_row: bytep,
        new_row: const_bytep,
    );
    #[link_name = "png_malloc"]
    pub fn malloc(
        ptr: const_structrp,
        size: alloc_size_t,
    ) -> voidp;
    #[link_name = "png_calloc"]
    pub fn calloc(
        ptr: const_structrp,
        size: alloc_size_t,
    ) -> voidp;
    #[link_name = "png_malloc_warn"]
    pub fn malloc_warn(
        ptr: const_structrp,
        size: alloc_size_t,
    ) -> voidp;
    #[link_name = "png_free"]
    pub fn free(
        ptr: const_structrp,
        ptr: voidp,
    );
    #[link_name = "png_free_data"]
    pub fn free_data(
        ptr: const_structrp,
        info_ptr: inforp,
        free_me: uint_32,
        num: c_int,
    );
    #[link_name = "png_data_freer"]
    pub fn data_freer(
        ptr: const_structrp,
        info_ptr: inforp,
        freer: c_int,
        mask: uint_32,
    );
    #[link_name = "png_malloc_default"]
    pub fn malloc_default(
        ptr: const_structrp,
        size: alloc_size_t,
    ) -> voidp;
    #[link_name = "png_free_default"]
    pub fn free_default(
        ptr: const_structrp,
        ptr: voidp,
    );
    #[link_name = "png_error"]
    pub fn error(
        ptr: const_structrp,
        error_message: const_charp,
    );
    #[link_name = "png_chunk_error"]
    pub fn chunk_error(
        ptr: const_structrp,
        error_message: const_charp,
    );
    #[link_name = "png_warning"]
    pub fn warning(
        ptr: const_structrp,
        warning_message: const_charp,
    );
    #[link_name = "png_chunk_warning"]
    pub fn chunk_warning(
        ptr: const_structrp,
        warning_message: const_charp,
    );
    #[link_name = "png_benign_error"]
    pub fn benign_error(
        ptr: const_structrp,
        warning_message: const_charp,
    );
    #[link_name = "png_chunk_benign_error"]
    pub fn chunk_benign_error(
        ptr: const_structrp,
        warning_message:
    const_charp,
    );
    #[link_name = "png_set_benign_errors"]
    pub fn set_benign_errors(
        ptr: structrp,
        allowed: c_int,
    );
    #[link_name = "png_get_valid"]
    pub fn get_valid(
        ptr: const_structrp,
        info_ptr: const_inforp,
        flag: uint_32,
    ) -> uint_32;
    #[link_name = "png_get_rowbytes"]
    pub fn get_rowbytes(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> usize;
    #[link_name = "png_get_rows"]
    pub fn get_rows(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> bytepp;
    #[link_name = "png_set_rows"]
    pub fn set_rows(
        ptr: const_structrp,
        info_ptr: inforp,
        row_pointers: bytepp,
    );
    #[link_name = "png_get_channels"]
    pub fn get_channels(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> byte;
    #[link_name = "png_get_image_width"]
    pub fn get_image_width(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> uint_32;
    #[link_name = "png_get_image_height"]
    pub fn get_image_height(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> uint_32;
    #[link_name = "png_get_bit_depth"]
    pub fn get_bit_depth(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> byte;
    #[link_name = "png_get_color_type"]
    pub fn get_color_type(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> byte;
    #[link_name = "png_get_filter_type"]
    pub fn get_filter_type(
        ptr: const_structrp,
        info_ptr: const_inforp,
    )
        -> byte;
        #[link_name = "png_get_interlace_type"]
    pub fn get_interlace_type(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> byte;
    #[link_name = "png_get_compression_type"]
    pub fn get_compression_type(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> byte;
    #[link_name = "png_get_pixels_per_meter"]
    pub fn get_pixels_per_meter(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> uint_32;
    #[link_name = "png_get_x_pixels_per_meter"]
    pub fn get_x_pixels_per_meter(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> uint_32;
    #[link_name = "png_get_y_pixels_per_meter"]
    pub fn get_y_pixels_per_meter(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> uint_32;
    #[link_name = "png_get_pixel_aspect_ratio"]
    pub fn get_pixel_aspect_ratio(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> f32;
    #[link_name = "png_get_pixel_aspect_ratio_fixed"]
    pub fn get_pixel_aspect_ratio_fixed(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> fixed_point;
    #[link_name = "png_get_x_offset_pixels"]
    pub fn get_x_offset_pixels(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> int_32;
    #[link_name = "png_get_y_offset_pixels"]
    pub fn get_y_offset_pixels(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> int_32;
    #[link_name = "png_get_x_offset_microns"]
    pub fn get_x_offset_microns(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> int_32;
    #[link_name = "png_get_y_offset_microns"]
    pub fn get_y_offset_microns(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> int_32;
    #[link_name = "png_get_signature"]
    pub fn get_signature(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> const_bytep;
    #[link_name = "png_get_bKGD"]
    pub fn get_bKGD(
        ptr: const_structrp,
        info_ptr: inforp,
        background: *mut color_16p,
    ) -> uint_32;
    #[link_name = "png_set_bKGD"]
    pub fn set_bKGD(
        ptr: const_structrp,
        info_ptr: inforp,
        background: const_color_16p,
    );
    #[link_name = "png_get_cHRM"]
    pub fn get_cHRM(
        ptr: const_structrp,
        info_ptr: const_inforp,
        white_x: *mut f64,
        white_y: *mut f64,
        red_x: *mut f64,
        red_y: *mut f64,
        green_x: *mut f64,
        green_y: *mut f64,
        blue_x: *mut f64,
        blue_y: *mut f64,
    ) -> uint_32;
    #[link_name = "png_get_cHRM_XYZ"]
    pub fn get_cHRM_XYZ(
        ptr: const_structrp,
        info_ptr: const_inforp,
        red_X: *mut f64,
        red_Y: *mut f64,
        red_Z: *mut f64,
        green_X: *mut f64,
        green_Y: *mut f64,
        green_Z: *mut f64,
        blue_X: *mut f64,
        blue_Y: *mut f64,
        blue_Z: *mut f64,
    ) -> uint_32;
    #[link_name = "png_get_cHRM_fixed"]
    pub fn get_cHRM_fixed(
        ptr: const_structrp,
        info_ptr: const_inforp,
        int_white_x: *mut fixed_point,
        int_white_y: *mut fixed_point,
        int_red_x: *mut fixed_point,
        int_red_y: *mut fixed_point,
        int_green_x: *mut fixed_point,
        int_green_y: *mut fixed_point,
        int_blue_x: *mut fixed_point,
        int_blue_y: *mut fixed_point,
    ) -> uint_32;
    #[link_name = "png_get_cHRM_XYZ_fixed"]
    pub fn get_cHRM_XYZ_fixed(
        ptr: const_structrp,
        info_ptr: const_inforp,
        int_red_X: *mut fixed_point,
        int_red_Y: *mut fixed_point,
        int_red_Z: *mut fixed_point,
        int_green_X: *mut fixed_point,
        int_green_Y: *mut fixed_point,
        int_green_Z: *mut fixed_point,
        int_blue_X: *mut fixed_point,
        int_blue_Y: *mut fixed_point,
        int_blue_Z: *mut fixed_point,
    ) -> uint_32;
    #[link_name = "png_set_cHRM"]
    pub fn set_cHRM(
        ptr: const_structrp,
        info_ptr: inforp,
        white_x: f64,
        white_y: f64,
        red_x: f64,
        red_y: f64,
        green_x: f64,
        green_y: f64,
        blue_x: f64,
        blue_y: f64,
    );
    #[link_name = "png_set_cHRM_XYZ"]
    pub fn set_cHRM_XYZ(
        ptr: const_structrp,
        info_ptr: inforp,
        red_X: f64,
        red_Y: f64,
        red_Z: f64,
        green_X: f64,
        green_Y: f64,
        green_Z: f64,
        blue_X: f64,
        blue_Y: f64,
        blue_Z: f64,
    );
    #[link_name = "png_set_cHRM_fixed"]
    pub fn set_cHRM_fixed(
        ptr: const_structrp,
        info_ptr: inforp,
        int_white_x: fixed_point,
        int_white_y: fixed_point,
        int_red_x: fixed_point,
        int_red_y: fixed_point,
        int_green_x: fixed_point,
        int_green_y: fixed_point,
        int_blue_x: fixed_point,
        int_blue_y: fixed_point,
    );
    #[link_name = "png_set_cHRM_XYZ_fixed"]
    pub fn set_cHRM_XYZ_fixed(
        ptr: const_structrp,
        info_ptr: inforp,
        int_red_X: fixed_point,
        int_red_Y: fixed_point,
        int_red_Z: fixed_point,
        int_green_X: fixed_point,
        int_green_Y: fixed_point,
        int_green_Z: fixed_point,
        int_blue_X: fixed_point,
        int_blue_Y: fixed_point,
        int_blue_Z: fixed_point,
    );
    #[link_name = "png_get_eXIf"]
    pub fn get_eXIf(
        ptr: const_structrp,
        info_ptr: inforp,
        exif: *mut bytep,
    ) -> uint_32;
    #[link_name = "png_set_eXIf"]
    pub fn set_eXIf(
        ptr: const_structrp,
        info_ptr: inforp,
        exif: bytep,
    );
    #[link_name = "png_get_eXIf_1"]
    pub fn get_eXIf_1(
        ptr: const_structrp,
        info_ptr: const_inforp,
        num_exif: *mut uint_32,
        exif: *mut bytep,
    ) -> uint_32;
    #[link_name = "png_set_eXIf_1"]
    pub fn set_eXIf_1(
        ptr: const_structrp,
        info_ptr: inforp,
        num_exif: uint_32,
        exif: bytep,
    );
    #[link_name = "png_get_gAMA"]
    pub fn get_gAMA(
        ptr: const_structrp,
        info_ptr: const_inforp,
        file_gamma: *mut f64,
    ) -> uint_32;
    #[link_name = "png_get_gAMA_fixed"]
    pub fn get_gAMA_fixed(
        ptr: const_structrp,
        info_ptr: const_inforp,
        int_file_gamma: *mut fixed_point,
    ) -> uint_32;
    #[link_name = "png_set_gAMA"]
    pub fn set_gAMA(
        ptr: const_structrp,
        info_ptr: inforp,
        file_gamma: f64,
    );
    #[link_name = "png_set_gAMA_fixed"]
    pub fn set_gAMA_fixed(
        ptr: const_structrp,
        info_ptr: inforp,
        int_file_gamma: fixed_point,
    );
    #[link_name = "png_get_hIST"]
    pub fn get_hIST(
        ptr: const_structrp,
        info_ptr: inforp,
        hist: *mut uint_16p,
    ) -> uint_32;
    #[link_name = "png_set_hIST"]
    pub fn set_hIST(
        ptr: const_structrp,
        info_ptr: inforp,
        hist: const_uint_16p,
    );
    #[link_name = "png_get_IHDR"]
    pub fn get_IHDR(
        ptr: const_structrp,
        info_ptr: const_inforp,
        width: *mut uint_32,
        height: *mut uint_32,
        bit_depth: *mut c_int,
        color_type: *mut c_int,
        interlace_method: *mut c_int,
        compression_method: *mut c_int,
        filter_method: *mut c_int,
    ) -> uint_32;
    #[link_name = "png_set_IHDR"]
    pub fn set_IHDR(
        ptr: const_structrp,
        info_ptr: inforp,
        width: uint_32,
        height: uint_32,
        bit_depth: c_int,
        color_type: c_int,
        interlace_method: c_int,
        compression_method: c_int,
        filter_method: c_int,
    );
    #[link_name = "png_get_oFFs"]
    pub fn get_oFFs(
        ptr: const_structrp,
        info_ptr: const_inforp,
        offset_x: *mut int_32,
        offset_y: *mut int_32,
        unit_type: *mut c_int,
    ) -> uint_32;
    #[link_name = "png_set_oFFs"]
    pub fn set_oFFs(
        ptr: const_structrp,
        info_ptr: inforp,
        offset_x: int_32,
        offset_y: int_32,
        unit_type: c_int,
    );
    #[link_name = "png_get_pCAL"]
    pub fn get_pCAL(
        ptr: const_structrp,
        info_ptr: inforp,
        purpose: *mut charp,
        X0: *mut int_32,
        X1: *mut int_32,
        type_: *mut c_int,
        nparams: *mut c_int,
        units: *mut charp,
        params: *mut charpp,
    ) -> uint_32;
    #[link_name = "png_set_pCAL"]
    pub fn set_pCAL(
        ptr: const_structrp,
        info_ptr: inforp,
        purpose: const_charp,
        X0: int_32,
        X1: int_32,
        type_: c_int,
        nparams: c_int,
        units: const_charp,
        params: charpp,
    );
    #[link_name = "png_get_pHYs"]
    pub fn get_pHYs(
        ptr: const_structrp,
        info_ptr: const_inforp,
        res_x: *mut uint_32,
        res_y: *mut uint_32,
        unit_type: *mut c_int,
    ) -> uint_32;
    #[link_name = "png_set_pHYs"]
    pub fn set_pHYs(
        ptr: const_structrp,
        info_ptr: inforp,
        res_x: uint_32,
        res_y: uint_32,
        unit_type: c_int,
    );
    #[link_name = "png_get_PLTE"]
    pub fn get_PLTE(
        ptr: const_structrp,
        info_ptr: inforp,
        palette: *mut colorp,
        num_palette: *mut c_int,
    ) -> uint_32;
    #[link_name = "png_set_PLTE"]
    pub fn set_PLTE(
        ptr: structrp,
        info_ptr: inforp,
        palette: const_colorp,
        num_palette: c_int,
    );
    #[link_name = "png_get_sBIT"]
    pub fn get_sBIT(
        ptr: const_structrp,
        info_ptr: inforp,
        sig_bit: *mut color_8p,
    ) -> uint_32;
    #[link_name = "png_set_sBIT"]
    pub fn set_sBIT(
        ptr: const_structrp,
        info_ptr: inforp,
        sig_bit: const_color_8p,
    );
    #[link_name = "png_get_sRGB"]
    pub fn get_sRGB(
        ptr: const_structrp,
        info_ptr: const_inforp,
        file_srgb_intent: *mut c_int,
    ) -> uint_32;
    #[link_name = "png_set_sRGB"]
    pub fn set_sRGB(
        ptr: const_structrp,
        info_ptr: inforp,
        srgb_intent: c_int,
    );
    #[link_name = "png_set_sRGB_gAMA_and_cHRM"]
    pub fn set_sRGB_gAMA_and_cHRM(
        ptr: const_structrp,
        info_ptr: inforp,
        srgb_intent: c_int,
    );
    #[link_name = "png_get_iCCP"]
    pub fn get_iCCP(
        ptr: const_structrp,
        info_ptr: inforp,
        name: charpp,
        compression_type: *mut c_int,
        profile: bytepp,
        proflen: *mut uint_32,
    ) -> uint_32;
    #[link_name = "png_set_iCCP"]
    pub fn set_iCCP(
        ptr: const_structrp,
        info_ptr: inforp,
        name: const_charp,
        compression_type: c_int,
        profile: const_bytep,
        proflen: uint_32,
    );
    #[link_name = "png_get_sPLT"]
    pub fn get_sPLT(
        ptr: const_structrp,
        info_ptr: inforp,
        entries: sPLT_tpp,
    ) -> c_int;
    #[link_name = "png_set_sPLT"]
    pub fn set_sPLT(
        ptr: const_structrp,
        info_ptr: inforp,
        entries: const_sPLT_tp,
        nentries: c_int,
    );
    #[link_name = "png_get_text"]
    pub fn get_text(
        ptr: const_structrp,
        info_ptr: inforp,
        text_ptr: *mut textp,
        num_text: *mut c_int,
    ) -> c_int;
    #[link_name = "png_set_text"]
    pub fn set_text(
        ptr: const_structrp,
        info_ptr: inforp,
        text_ptr: const_textp,
        num_text: c_int,
    );
    #[link_name = "png_get_tIME"]
    pub fn get_tIME(
        ptr: const_structrp,
        info_ptr: inforp,
        mod_time: *mut timep,
    ) -> uint_32;
    #[link_name = "png_set_tIME"]
    pub fn set_tIME(
        ptr: const_structrp,
        info_ptr: inforp,
        mod_time: const_timep,
    );
    #[link_name = "png_get_tRNS"]
    pub fn get_tRNS(
        ptr: const_structrp,
        info_ptr: inforp,
        trans_alpha: *mut bytep,
        num_trans: *mut c_int,
        trans_color: *mut color_16p,
    ) -> uint_32;
    #[link_name = "png_set_tRNS"]
    pub fn set_tRNS(
        ptr: structrp,
        info_ptr: inforp,
        trans_alpha: const_bytep,
        num_trans: c_int,
        trans_color: const_color_16p,
    );
    #[link_name = "png_get_sCAL"]
    pub fn get_sCAL(
        ptr: const_structrp,
        info_ptr: const_inforp,
        unit: *mut c_int,
        width: *mut f64,
        height: *mut f64,
    ) -> uint_32;
    #[link_name = "png_get_sCAL_fixed"]
    pub fn get_sCAL_fixed(
        ptr: const_structrp,
        info_ptr: const_inforp,
        unit: *mut c_int,
        width: *mut fixed_point,
        height: *mut fixed_point,
    ) -> uint_32;
    #[link_name = "png_get_sCAL_s"]
    pub fn get_sCAL_s(
        ptr: const_structrp,
        info_ptr: const_inforp,
        unit: *mut c_int,
        swidth: charpp,
        sheight: charpp,
    ) -> uint_32;
    #[link_name = "png_set_sCAL"]
    pub fn set_sCAL(
        ptr: const_structrp,
        info_ptr: inforp,
        unit: c_int,
        width: f64,
        height: f64,
    );
    #[link_name = "png_set_sCAL_fixed"]
    pub fn set_sCAL_fixed(
        ptr: const_structrp,
        info_ptr: inforp,
        unit: c_int,
        width: fixed_point,
        height: fixed_point,
    );
    #[link_name = "png_set_sCAL_s"]
    pub fn set_sCAL_s(
        ptr: const_structrp,
        info_ptr: inforp,
        unit: c_int,
        swidth: const_charp,
        sheight: const_charp,
    );
    #[link_name = "png_set_keep_unknown_chunks"]
    pub fn set_keep_unknown_chunks(
        ptr: structrp,
        keep: c_int,
        chunk_list: const_bytep,
        num_chunks: c_int,
    );
    #[link_name = "png_handle_as_unknown"]
    pub fn handle_as_unknown(
        ptr: const_structrp,
        chunk_name: const_bytep,
    ) -> c_int;
    #[link_name = "png_set_unknown_chunks"]
    pub fn set_unknown_chunks(
        ptr: const_structrp,
        info_ptr: inforp,
        unknowns: const_unknown_chunkp,
        num_unknowns: c_int,
    );
    #[link_name = "png_set_unknown_chunk_location"]
    pub fn set_unknown_chunk_location(
        ptr: const_structrp,
        info_ptr: inforp,
        chunk: c_int,
        location: c_int,
    );
    #[link_name = "png_get_unknown_chunks"]
    pub fn get_unknown_chunks(
        ptr: const_structrp,
        info_ptr: inforp,
        entries: unknown_chunkpp,
    ) -> c_int;
    #[link_name = "png_set_invalid"]
    pub fn set_invalid(
        ptr: const_structrp,
        info_ptr: inforp,
        mask: c_int,
    );
    #[link_name = "png_read_png"]
    pub fn read_png(
        ptr: structrp,
        info_ptr: inforp,
        transforms: c_int,
        params: voidp,
    );
    #[link_name = "png_write_png"]
    pub fn write_png(
        ptr: structrp,
        info_ptr: inforp,
        transforms: c_int,
        params: voidp,
    );
    #[link_name = "png_get_copyright"]
    pub fn get_copyright(
        ptr: const_structrp,
    ) -> const_charp;
    #[link_name = "png_get_header_ver"]
    pub fn get_header_ver(
        ptr: const_structrp,
    ) -> const_charp;
    #[link_name = "png_get_header_version"]
    pub fn get_header_version(
        ptr: const_structrp,
    ) -> const_charp;
    #[link_name = "png_get_libpng_ver"]
    pub fn get_libpng_ver(
        ptr: const_structrp,
    ) -> const_charp;
    #[link_name = "png_permit_mng_features"]
    pub fn permit_mng_features(
        ptr: structrp,
        mng_features_permitted: uint_32,
    ) -> uint_32;
    #[link_name = "png_set_user_limits"]
    pub fn set_user_limits(
        ptr: structrp,
        user_width_max: uint_32,
        user_height_max: uint_32,
    );
    #[link_name = "png_get_user_width_max"]
    pub fn get_user_width_max(
        ptr: const_structrp,
    ) -> uint_32;
    #[link_name = "png_get_user_height_max"]
    pub fn get_user_height_max(
        ptr: const_structrp,
    ) -> uint_32;
    #[link_name = "png_set_chunk_cache_max"]
    pub fn set_chunk_cache_max(
        ptr: structrp,
        user_chunk_cache_max: uint_32,
    );
    #[link_name = "png_get_chunk_cache_max"]
    pub fn get_chunk_cache_max(
        ptr: const_structrp,
    ) -> uint_32;
    #[link_name = "png_set_chunk_malloc_max"]
    pub fn set_chunk_malloc_max(
        ptr: structrp,
        user_chunk_cache_max:
    alloc_size_t,
    );
    #[link_name = "png_get_chunk_malloc_max"]
    pub fn get_chunk_malloc_max(
        ptr: const_structrp,
    ) -> alloc_size_t;
    #[link_name = "png_get_pixels_per_inch"]
    pub fn get_pixels_per_inch(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> uint_32;
    #[link_name = "png_get_x_pixels_per_inch"]
    pub fn get_x_pixels_per_inch(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> uint_32;
    #[link_name = "png_get_y_pixels_per_inch"]
    pub fn get_y_pixels_per_inch(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> uint_32;
    #[link_name = "png_get_x_offset_inches"]
    pub fn get_x_offset_inches(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> f32;
    #[link_name = "png_get_x_offset_inches_fixed"]
    pub fn get_x_offset_inches_fixed(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> fixed_point;
    #[link_name = "png_get_y_offset_inches"]
    pub fn get_y_offset_inches(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> f32;
    #[link_name = "png_get_y_offset_inches_fixed"]
    pub fn get_y_offset_inches_fixed(
        ptr: const_structrp,
        info_ptr: const_inforp,
    ) -> fixed_point;
    #[link_name = "png_get_pHYs_dpi"]
    pub fn get_pHYs_dpi(
        ptr: const_structrp,
        info_ptr: const_inforp,
        res_x: *mut uint_32,
        res_y: *mut uint_32,
        unit_type: *mut c_int,
    ) -> uint_32;
    #[link_name = "png_get_io_state"]
    pub fn get_io_state(
        ptr: const_structrp,
    ) -> uint_32;
    #[link_name = "png_get_io_chunk_type"]
    pub fn get_io_chunk_type(
        ptr: const_structrp,
    ) -> uint_32;
    #[link_name = "png_get_uint_32"]
    pub fn get_uint_32(
        buf: const_bytep,
    ) -> uint_32;
    #[link_name = "png_get_uint_16"]
    pub fn get_uint_16(
        buf: const_bytep,
    ) -> uint_16;
    #[link_name = "png_get_int_32"]
    pub fn get_int_32(
        buf: const_bytep,
    ) -> int_32;
    #[link_name = "png_get_uint_31"]
    pub fn get_uint_31(
        ptr: const_structrp,
        buf: const_bytep,
    ) -> uint_32;
    #[link_name = "png_save_uint_32"]
    pub fn save_uint_32(
        buf: bytep,
        i: uint_32,
    );
    #[link_name = "png_save_int_32"]
    pub fn save_int_32(
        buf: bytep,
        i: int_32,
    );
    #[link_name = "png_save_uint_16"]
    pub fn save_uint_16(
        buf: bytep,
        i: c_uint,
    );
    #[link_name = "png_set_check_for_invalid_index"]
    pub fn set_check_for_invalid_index(
        ptr: structrp,
        allowed: c_int,
    );
    #[link_name = "png_get_palette_max"]
    pub fn get_palette_max(
        ptr: const_structp,
        info_ptr: const_infop,
    ) -> c_int;
    #[link_name = "png_image_begin_read_from_file"]
    pub fn image_begin_read_from_file(
        image: imagep,
        file_name: *const c_char,
    ) -> c_int;
    #[link_name = "png_image_begin_read_from_stdio"]
    pub fn image_begin_read_from_stdio(
        image: imagep,
        file: *mut FILE,
    ) -> c_int;
    #[link_name = "png_image_begin_read_from_memory"]
    pub fn image_begin_read_from_memory(
        image: imagep,
        memory: const_voidp,
        size: usize,
    ) -> c_int;
    #[link_name = "png_image_finish_read"]
    pub fn image_finish_read(
        image: imagep,
        background: const_colorp,
        buffer: *mut c_void,
        row_stride: int_32,
        colormap: *mut c_void,
    ) -> c_int;
    #[link_name = "png_image_free"]
    pub fn image_free(
        image: imagep,
    );
    #[link_name = "png_image_write_to_file"]
    pub fn image_write_to_file(
        image: imagep,
        file: *const c_char,
        convert_to_8bit: c_int,
        buffer: *const c_void,
        row_stride: int_32,
        colormap: *const c_void,
    ) -> c_int;
    #[link_name = "png_image_write_to_stdio"]
    pub fn image_write_to_stdio(
        image: imagep,
        file: *mut FILE,
        convert_to_8_bit: c_int,
        buffer: *const c_void,
        row_stride: int_32,
        colormap: *const c_void,
    ) -> c_int;
    #[link_name = "png_image_write_to_memory"]
    pub fn image_write_to_memory(
        image: imagep,
        memory: *mut c_void,
        memory_bytes: *mut alloc_size_t,
        convert_to_8_bit: c_int,
        buffer: *const c_void,
        row_stride: int_32,
        colormap: *const c_void,
    ) -> c_int;
    #[link_name = "png_set_option"]
    pub fn set_option(
        ptr: structrp,
        option: c_int,
        onoff: c_int,
    ) -> c_int;
    #[link_name = "png_get_acTL"]
    pub fn get_acTL(
        ptr: structp,
        info_ptr: infop,
        num_frames: *mut uint_32,
        num_plays: *mut uint_32,
    ) -> uint_32;
    #[link_name = "png_set_acTL"]
    pub fn set_acTL(
        ptr: structp,
        info_ptr: infop,
        num_frames: uint_32,
        num_plays: uint_32,
    ) -> uint_32;
    #[link_name = "png_get_num_frames"]
    pub fn get_num_frames(
        ptr: structp,
        info_ptr: infop,
    ) -> uint_32;
    #[link_name = "png_get_num_plays"]
    pub fn get_num_plays(
        ptr: structp,
        info_ptr: infop,
    ) -> uint_32;
    #[link_name = "png_get_next_frame_fcTL"]
    pub fn get_next_frame_fcTL(
        ptr: structp,
        info_ptr: infop,
        width: *mut uint_32,
        height: *mut uint_32,
        x_offset: *mut uint_32,
        y_offset: *mut uint_32,
        delay_num: *mut uint_16,
        delay_den: *mut uint_16,
        dispose_op: *mut byte,
        blend_op: *mut byte,
    ) -> uint_32;
    #[link_name = "png_set_next_frame_fcTL"]
    pub fn set_next_frame_fcTL(
        ptr: structp,
        info_ptr: infop,
        width: uint_32,
        height: uint_32,
        x_offset: uint_32,
        y_offset: uint_32,
        delay_num: uint_16,
        delay_den: uint_16,
        dispose_op: byte,
        blend_op: byte,
    ) -> uint_32;
    #[link_name = "png_get_next_frame_width"]
    pub fn get_next_frame_width(
        ptr: structp,
        info_ptr: infop,
    ) -> uint_32;
    #[link_name = "png_get_next_frame_height"]
    pub fn get_next_frame_height(
        ptr: structp,
        info_ptr: infop,
    ) -> uint_32;
    #[link_name = "png_get_next_frame_x_offset"]
    pub fn get_next_frame_x_offset(
        ptr: structp,
        info_ptr: infop,
    ) -> uint_32;
    #[link_name = "png_get_next_frame_y_offset"]
    pub fn get_next_frame_y_offset(
        ptr: structp,
        info_ptr: infop,
    ) -> uint_32;
    #[link_name = "png_get_next_frame_delay_num"]
    pub fn get_next_frame_delay_num(
        ptr: structp,
        info_ptr: infop,
    ) -> uint_16;
    #[link_name = "png_get_next_frame_delay_den"]
    pub fn get_next_frame_delay_den(
        ptr: structp,
        info_ptr: infop,
    ) -> uint_16;
    #[link_name = "png_get_next_frame_dispose_op"]
    pub fn get_next_frame_dispose_op(
        ptr: structp,
        info_ptr: infop,
    ) -> byte;
    #[link_name = "png_get_next_frame_blend_op"]
    pub fn get_next_frame_blend_op(
        ptr: structp,
        info_ptr: infop,
    ) -> byte;
    #[link_name = "png_get_first_frame_is_hidden"]
    pub fn get_first_frame_is_hidden(
        ptr: structp,
        info_ptr: infop,
    ) -> byte;
    #[link_name = "png_set_first_frame_is_hidden"]
    pub fn set_first_frame_is_hidden(
        ptr: structp,
        info_ptr: infop,
        is_hidden: byte,
    ) -> uint_32;
    #[link_name = "png_read_frame_head"]
    pub fn read_frame_head(
        ptr: structp,
        info_ptr: infop,
    );
    #[link_name = "png_set_progressive_frame_fn"]
    pub fn set_progressive_frame_fn(
        ptr: structp,
        frame_info_fn: progressive_frame_ptr,
        frame_end_fn: progressive_frame_ptr,
    );
    #[link_name = "png_write_frame_head"]
    pub fn write_frame_head(
        ptr: structp,
        info_ptr: infop,
        row_pointers: bytepp,
        width: uint_32,
        height: uint_32,
        x_offset: uint_32,
        y_offset: uint_32,
        delay_num: uint_16,
        delay_den: uint_16,
        dispose_op: byte,
        blend_op: byte,
    );
    #[link_name = "png_write_frame_tail"]
    pub fn write_frame_tail(
        ptr: structp,
        info_ptr: infop,
    );
}
