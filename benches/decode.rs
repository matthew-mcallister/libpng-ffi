use std::env;
use std::ffi::OsString;
use std::fs;
use std::io;
use std::os::raw::*;
use std::ptr;

use criterion::{Criterion, black_box, criterion_group, criterion_main};
use libpng_ffi as png;

// Shouldn't matter at the PNG decoding stage, only final output.
const DISPLAY_GAMMA: f64 = 2.2;

macro_rules! c_str {
    ($str:expr) => {
        concat!($str, "\0") as *const str as *const c_char
    }
}

unsafe extern "C" fn error_fn(_ptr: png::structp, msg: png::const_charp) {
    let cstr = std::ffi::CStr::from_ptr(msg);
    panic!("libpng error: {:?}", cstr);
}

unsafe extern "C" fn warning_fn(_ptr: png::structp, msg: png::const_charp) {
    let cstr = std::ffi::CStr::from_ptr(msg);
    eprintln!("libpng warning: {:?}", cstr);
}

unsafe extern "C" fn read_fn(png: png::structp, out: png::bytep, size: usize) {
    let io_ptr = png::get_io_ptr(png) as *mut *mut dyn io::Read;
    let src = &mut **io_ptr;

    let dst = std::slice::from_raw_parts_mut(out as *mut u8, size);

    if let Err(e) = src.read_exact(dst) {
        panic!("libpng io error: {}", e);
    }
}

type RGBA8 = [u8; 4];

unsafe fn decode_png<R: io::Read>(src: R) -> Vec<RGBA8> {
    let (mut readp, mut infop) = (0 as _, 0 as _);
    let res = decode_png_inner(src, &mut readp, &mut infop);
    png::destroy_read_struct(
        &mut readp as _,
        &mut infop as _,
        ptr::null_mut(),
    );
    res
}

unsafe fn decode_png_inner<R: io::Read>(
    src: R,
    readp: &mut png::structp,
    infop: &mut png::infop,
) -> Vec<RGBA8> {
    *readp = png::create_read_struct(
        c_str!("1.6.36"),
        ptr::null_mut(),
        Some(error_fn),
        Some(warning_fn),
    );
    let readp = *readp;
    assert_ne!(readp, 0 as _);

    *infop = png::create_info_struct(readp);
    let infop = *infop;
    assert_ne!(infop, 0 as _);

    // double indirection: io_ptr points to Box<dyn io::Read>
    let mut src: Box<dyn io::Read> = Box::new(src);
    png::set_read_fn(readp, &mut src as *mut _ as _, Some(read_fn));

    // Use realistic decoding parameters
    png::set_alpha_mode(readp, png::ALPHA_PREMULTIPLIED!(), DISPLAY_GAMMA);

    png::read_info(readp, infop);
    let (mut width, mut height) = Default::default();
    png::get_IHDR(
        readp,
        infop,
        &mut width as _,
        &mut height as _,
        ptr::null_mut(),
        ptr::null_mut(),
        ptr::null_mut(),
        ptr::null_mut(),
        ptr::null_mut(),
    );

    png::set_expand(readp);
    png::set_gray_to_rgb(readp);
    png::set_add_alpha(readp, u8::max_value() as _, png::FILLER_BEFORE!());

    let (width, height) = (width as usize, height as usize);
    let mut res: Vec<RGBA8> = Vec::with_capacity(width * height);
    res.set_len(width * height);

    let mut rows: Vec<_> = (0..height)
        .map(|row| &mut res[width * row] as *mut RGBA8 as png::bytep)
        .collect();

    png::read_image(readp, rows.as_mut_ptr());

    res
}

fn bench_decode(c: &mut Criterion) {
    let filename: OsString = env::var_os("BENCH_IMAGE")
        .expect("`BENCH_IMAGE` variable is required")
        .into();

    unsafe {
        c.bench_function("decode", move |b| b.iter(|| {
            let file = fs::File::open(black_box(&filename)).unwrap();
            black_box(decode_png(file));
        }));
    }
}

criterion_group!(benches, bench_decode);
criterion_main!(benches);
